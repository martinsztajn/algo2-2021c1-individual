#include "Lista.h"

Lista::Lista() {
    header = new Nodo;
    trailer = new Nodo;
    header->next = trailer;
    trailer->prev = header;
    header->prev = nullptr;
    trailer->next = nullptr;
    header->data = 0;
    trailer->data = 0;
    _longitud = 0;
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

void Lista::eliminarPrimero(){
    Nodo *nd = header->next;
    delete header;
    header = nd;
    header->prev = nullptr;

    _longitud--;
}

Lista::~Lista() {
    // Completar
    if(longitud() > 0)
    {
        while(header->next != trailer)
        {
            eliminarPrimero();
        }

        _longitud = 0;
    }
    delete header;
    delete trailer;

}

Lista& Lista::operator=(const Lista& aCopiar) {
    if(longitud() > 0)
    {
        while(header->next != trailer)
        {
            eliminarPrimero();
        }
        _longitud = 0;
    }
    int i = 0;
    while (i < aCopiar.longitud())
    {
        agregarAtras(aCopiar.iesimo(i));
        i++;
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
        Nodo *nd = new Nodo;
        nd->data = elem;
        nd->prev = header;
        nd->next = header->next;

        header->next->prev = nd;
        header->next = nd;
        _longitud++;

}

void Lista::agregarAtras(const int& elem) {
    Nodo *nd = new Nodo;
    nd->data = elem;
    nd->prev = trailer->prev;
    nd->next = trailer;

    trailer->prev->next = nd;
    trailer->prev = nd;
    _longitud++;
}

void Lista::eliminar(Nat i) {
    Nodo *h = header->next;
    while (i != 0){
        h = h->next;
        i--;
    }
    h->prev->next = h->next;
    h->next->prev = h->prev;

    delete h;
    _longitud--;
}

int Lista::longitud() const {
    // Completar
    return _longitud;
}

const int& Lista::iesimo(Nat i) const {
    Nodo *h = header->next;

    while (i != 0){
        h = h->next;
        i--;
    }

    return h->data;
}

int& Lista::iesimo(Nat i) {
    Nodo *h = header->next;
    while (i != 0){
        h = h->next;
        i--;
    }

    return h->data;
}


