#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}




// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
    public:
        Fecha(int mes, int dia);
        int mes();
        int dia();
        bool operator==(Fecha o);
        void incrementar_dia();

private:
        int dia_;
        int mes_;


    // Completar declaraciones funciones

};

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}


Fecha::Fecha(int mes, int dia) :  mes_(mes), dia_(dia) {};

int Fecha::dia() {
    return dia_;
}

int Fecha::mes() {
    return mes_;
}




#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    return (igual_dia && igual_mes);
}
void Fecha::incrementar_dia() {
    if (dias_en_mes(this->mes_) ==  this->dia_)
    {
        this->dia_ = 1;
        if (this->mes_ == 11)
        {
            this->mes_ = 0;
        }
        else{
            this->mes_++;
        }
    }
    else
    {
        this->dia_++;
    }

}
#endif


// Ejercicio 11, 12

// Clase Horario
class Horario {
public:
    Horario(uint min, uint hora);
    uint min();
    uint hora();
    bool operator==(Horario o);
    bool operator<(Horario h);

private:
    uint min_;
    uint hora_;

};

Horario::Horario(uint min, uint hora) : min_(min), hora_(hora) {};

uint Horario::min() {
    return min_;
}

uint Horario::hora() {
    return hora_;
}
bool Horario::operator==(Horario o) {
    bool igual_min = this->min() == o.min();
    bool igual_hora = this->hora() == o.hora();
    return (igual_min && igual_hora);
}
ostream& operator<<(ostream& os, Horario o) {
    os <<  o.hora() << ":" << o.min();
    return os;
}

bool Horario::operator<(Horario h) {
    if (this->hora() < h.hora())
    {
        return true;
    }
    else if(this->hora() == h.hora())
    {
        if (this->min() <= h.min())
        {
            return true;
        }
    }
    return false;
}



// Ejercicio 13
class Recordatorio {
public:
    Recordatorio(Fecha f, Horario h, string str);
    Fecha f();
    Horario h();
    string str();

private:
    Fecha f_;
    Horario h_;
    string str_;
};

Recordatorio::Recordatorio(Fecha f, Horario h, string str) :  f_(f), h_(h), str_(str) {};

Fecha Recordatorio::f() {
    return f_;
}
Horario Recordatorio::h() {
    return h_;
}
string Recordatorio::str() {
    return str_;
}

ostream& operator<<(ostream& os, Recordatorio o) {
    os <<  o.str() << " @ " << o.f().dia() << "/" << o.f().mes() << " " << o.h().hora() << ":" << o.h().min();
    return os;
}


// Ejercicio 14



class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();

private:
    Fecha hoy_;
    Fecha fecha_inicial_;
    list<Recordatorio> rec_;

};

void Agenda::agregar_recordatorio(Recordatorio rec){
    this->rec_.push_back(rec);
}


Agenda::Agenda(Fecha fecha_inicial) : fecha_inicial_(fecha_inicial),  hoy_(fecha_inicial) {};

Fecha Agenda::hoy(){
    return hoy_;
}

list<Recordatorio> Agenda::recordatorios_de_hoy(){
    vector<Recordatorio> r1;
    for (Recordatorio r : rec_) {
            if (r.f() == hoy())
            {
                r1.push_back(r);
            }
    }
    if (r1.size() >= 2) {
        for (int i = 0; i < r1.size() - 1; i++) {
            for (int j = i + 1; j < r1.size(); j++) {
                if (r1[i].h().hora() > r1[j].h().hora() ||
                    (r1[i].h().hora() == r1[j].h().hora() && r1[i].h().min() > r1[j].h().min())) {
                    Recordatorio aux = r1[i];
                    r1[i] = r1[j];
                    r1[j] = aux;
                }
            }
        }
    }
    list<Recordatorio> l1(r1.begin(), r1.end());
    return l1;
}

void Agenda::incrementar_dia() {
    hoy_.incrementar_dia();
}


ostream& operator<<(ostream& os, Agenda a) {
    os <<  a.hoy() <<  endl;
    os << "=====" << endl;
    list<Recordatorio> reco =  a.recordatorios_de_hoy();
    for (Recordatorio r : reco) {
        os << r << endl;
    }
    return os;
}
