#ifndef STRING_MAP_H_
#define STRING_MAP_H_

#include <string>

using namespace std;
using Palabra = string;

template<typename T>
class string_map {
public:
   

    string_map();
    string_map(const string_map<T>& aCopiar);
    string_map& operator=(const string_map& d);
    ~string_map();
    void insert(const pair<Palabra, T>& clave_valor);
    int count(const Palabra &key) const;
    const T& at(const Palabra& key) const;
    T& at(const Palabra& key);
    void erase(const Palabra& key);
    int size() const;
    bool empty() const;\
    T &operator[](const string &key);

private:

    struct Nodo {
        vector<Nodo*> siguientes;
        T* definicion;
        Nodo();
        Nodo(T* def);
        int estaDefinido(const string& clave);
        const T& obtener(const Palabra& clave);
        T& obtener2(const Palabra& clave);
        void nuevaRama(Palabra clave, T sig);
        pair<Nodo*, Palabra> ultimoNodo(Palabra clave);
        void copiar(Nodo* aCopiar);
        int numRamas();
    };

    Nodo* raiz;
    int _size;
    void destruir(Nodo* nodo);
};

#include "string_map.hpp"

#endif // STRING_MAP_H_
