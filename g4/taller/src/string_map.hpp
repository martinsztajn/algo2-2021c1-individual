
template <typename T>
string_map<T>::Nodo::Nodo(): siguientes(256, NULL), definicion(NULL){}

template <typename T>
string_map<T>::Nodo::Nodo(T* def): siguientes(256, NULL), definicion(def){}


template <typename T>
string_map<T>::string_map(){
    _size = 0;
    raiz = NULL;
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; }


template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& strMap) {
    _size = strMap._size;
    raiz = new Nodo();
    if(strMap.raiz != NULL){
        raiz->copiar(strMap.raiz);
    }
    return *this;
}
template <typename T>
void string_map<T>::Nodo::copiar(Nodo* aCopiar) {
    if(aCopiar->definicion != NULL){
        this->definicion = new T(*(aCopiar->definicion));
    }
    for(int i = 0; i< 256; i++){
        if(aCopiar->siguientes[i] != NULL){
            this->siguientes[i] = new Nodo();
            (this->siguientes[i])->copiar(aCopiar->siguientes[i]);
        }
    }
}

template <typename T>
string_map<T>::~string_map(){
    destruir(raiz);
}

template <typename T>
void string_map<T>::destruir(Nodo* nodo){
    if(nodo != NULL){
        for(int i = 0; i < 256; i++){
            if(nodo->siguientes[i] != NULL){
                destruir(nodo->siguientes[i]);
                nodo->siguientes[i] = NULL;
            }
        }
        if(nodo->definicion != NULL){
            delete nodo->definicion;
            nodo->definicion = NULL;
        }
        delete nodo;
    }
}

template <typename T>
void string_map<T>::insert(const pair<string, T>& clave){
    if(raiz == NULL){
        raiz = new Nodo();
        raiz->nuevaRama(clave.first, clave.second);
        _size = 1;
    }else{
        pair<Nodo*, Palabra> ultimoNodo =raiz->ultimoNodo(clave.first);
        if(ultimoNodo.second.size()==0){
            if(ultimoNodo.first->definicion != NULL){
                delete ultimoNodo.first->definicion;
                ultimoNodo.first->definicion = NULL;
            }
            ultimoNodo.first->definicion = new T(clave.second);
        }else{
            ultimoNodo.first->nuevaRama(ultimoNodo.second, clave.second);
        }
        _size++;
    }
}
template <typename T>
void string_map<T>::Nodo::nuevaRama(string clave, T sig){
    this->siguientes[int(clave[0])] = new Nodo();
    if(clave.size() == 1){
        (this->siguientes[int(clave[0])])->definicion = new T(sig);
    }else{
        (this->siguientes[int(clave[0])])->nuevaRama(clave.erase(0,1), sig);
    }
}
template <typename T>
pair<typename string_map<T>::Nodo*, string> string_map<T>::Nodo::ultimoNodo(string clave){
    if(clave.length()==0){
        return make_pair(this, clave);
    }else{
        if (this->siguientes[int(clave[0])] == NULL) {
            return make_pair(this, clave);
        }else{
            return (this->siguientes[int(clave[0])])->ultimoNodo(clave.erase(0,1));
        }
    }
}


template <typename T>
int string_map<T>::count(const string& clave) const{
    if(raiz == NULL){
        return 0;
    }else{
        return raiz->estaDefinido(clave);
    }
}

template <typename T>
int string_map<T>::Nodo::estaDefinido(const string& clave){
    if(clave.length() == 0) {
        // tiene o no sig
        if(this->definicion == NULL){
            return 0;
        }else{
            return 1;
        }
    }else if(this->siguientes[int(clave[0])] == NULL){
        // falta la rama -> no está def
        return 0;
    }else{
        string newClave = clave;
        return (this->siguientes[int(clave[0])])->estaDefinido(newClave.erase(0,1));
    }
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    return raiz->obtener(clave);
}
template <typename T>
const T& string_map<T>::Nodo::obtener(const string& clave){
    if(clave.length() == 0){
        return *(this->definicion);
    }else{
        string newClave = clave;
        return (this->siguientes[int(clave[0])])->obtener2(newClave.erase(0, 1));
    }
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    return raiz->obtener2(clave);
}

template <typename T>
T& string_map<T>::Nodo::obtener2(const string& clave){
    if(clave.length() == 0){
        return *(this->definicion);
    }else{
        string newClave = clave;
        return (this->siguientes[int(clave[0])])->obtener2(newClave.erase(0, 1));
    }
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    Nodo* actual = raiz;
    Nodo* ultimo = raiz;
    int indice = 0;
    for(int i = 0; i < clave.length(); i++){
        actual = actual->siguientes[clave[i]];
        if((i < clave.length()-1) && ((actual->definicion != NULL) || (actual->numRamas() > 1))){
            ultimo = actual;
            indice = i;
        }
    }
    if(actual->numRamas() > 0){
        delete actual->definicion;
        actual->definicion = NULL;
    }else{
        delete actual->definicion;
        actual->definicion = NULL;
        destruir(ultimo->siguientes[int(clave[indice])]);
        ultimo->siguientes[int(clave[indice])] = NULL;
    }
}
template <typename T>
int string_map<T>::Nodo::numRamas(){
    int cantDeHijos = 0;
    for(int i = 0; i< 256 ; i++){
        if(this->siguientes[i] != NULL){
            cantDeHijos++;
        }
    }
    return cantDeHijos;
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    return raiz == NULL;
}