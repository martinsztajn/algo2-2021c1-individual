
template <class T>
Conjunto<T>::Conjunto() {
    _cardinal = 0;
    _raiz = NULL;
}

template <class T>
Conjunto<T>::~Conjunto() {
    Nodo * actual = _raiz;
    if(actual != NULL && cardinal()>0){
        Eliminar(actual);
    }
    _raiz = NULL; //POR LAS DUDAS, QUIZAS NO VAYA

}

template <class T>
void Conjunto<T>::Eliminar(Nodo * n){
    if(n->izq != NULL){
        Eliminar(n->izq);
    }
    if(n->der != NULL){
        Eliminar(n->der);
    }
    delete n;
    _cardinal--;
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    bool res = false;
    Nodo* r = _raiz;
    if(cardinal()>0){
        while(r != NULL && r->valor != clave){
            if (r->valor > clave){
                r = r->izq;
            } else {
                r = r->der;
            }
        }
        res = res || r != NULL;
    }
    return res;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    Nodo* iterar = _raiz;
    Nodo* elemPadre = NULL;
    if(!pertenece(clave)){
        Nodo *nuevo = new Nodo(clave);
        if (cardinal() == 0) {
            _raiz = nuevo;
        } else {
            while (iterar != nullptr) {
                elemPadre = iterar;
                nuevo->padre = elemPadre;
                if (iterar->valor > clave) {
                    iterar = iterar->izq;
                } else {
                    iterar = iterar->der;
                }
            }
            if(elemPadre->valor > clave) {
                nuevo->padre->izq = nuevo;
                iterar = nuevo;
            } else {
                nuevo->padre->der = nuevo;
                iterar = nuevo;
            }
        }
        _cardinal++;
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    Nodo* iterar = _raiz;
    while(iterar != NULL && iterar->valor != clave){
        if (iterar->valor > clave){
            iterar = iterar->izq;
        } else {
            iterar = iterar->der;
        }
    }
    //SI EL ELEMENTO QUE QUIERO ELIMINAR SE ENCUENTRA EN EL CONJUNTO
    if(iterar != NULL){
        //3 Casos
        if(iterar->izq == NULL && iterar->der == NULL){
            //ES HOJA
            EliminarHoja(iterar);
        } else if (iterar->izq == NULL || iterar->der == NULL){
            //TIENE UN SOLO HIJO
            EliminarPadreUnHijo(iterar);
        } else {
            //TIENE DOS HIJOS
            //BUSCAMOS EL PREDECESOR INMEDIATO: EL ELEMENTO MÁS A LA DERECHA, DE LOS DE LA IZQUIERA DE _ACTUAL
            Nodo* predI = iterar->izq; // SI LLEGAMOS HASTA ACA, SABEMOS QUE ESTO NO SE INDEFINE
            while(predI->der != NULL){
                predI = predI->der;
            }
            //COPIAMOS EL PREDECESOR INMEDIATO EN EL LUGAR DEL NODO QUE QUEREMOS BORRAR (LO BORRAMOS REEMPLAZANDOLO CON predI)
            iterar->valor = predI->valor;
            iterar = predI;
            //BORRAMOS predI: REPETIMOS EL PROCESO DE ARRIBA PERO AHORA CON predI
            if(iterar->izq == NULL && iterar->der == NULL){
                //PRED ES HOJA
                EliminarHoja(iterar);
            } else {
                //PRED TIENE UN SOLO HIJO
                EliminarPadreUnHijo(iterar);
            }
        }
        _cardinal--;
    }
}

template <class T>
void Conjunto<T>::EliminarHoja(Nodo* actual){
    if(actual->padre != NULL){
        //NO ESTOY ELIMINANDO A LA RAIZ
        if (actual->padre->izq == actual){
            //ERA HIJO POR IZQ
            actual->padre->izq = NULL;
            delete actual;
        } else {
            //ERA HIJO POR DER
            actual->padre->der = NULL;
            delete actual;
        }
    } else {
        delete actual;
        //delete r;
    }
}

template <class T>
void Conjunto<T>::EliminarPadreUnHijo(Nodo * actual) {
    if (actual->padre != NULL) {
        //NO ESTOY ELIMINANDO A LA RAIZ
        if (actual->izq == NULL) {
            //TIENE UN HIJO A LA DERECHA
            if(actual->padre->izq == actual){
                //ES HIJO A LA IZQUIERDA
                actual->padre->izq = actual->der;
            } else {
                //ES HIJO A LA DERECHA
                actual->padre->der = actual->der;
            }
            actual->der->padre = actual->padre;
        } else {
            //TIENE UN HIJO A LA IZQUIERDA
            if(actual->padre->izq == actual){
                //ES HIJO A LA IZQUIERDA
                actual->padre->izq = actual->izq;
            } else {
                //ES HIJO A LA DERECHA
                actual->padre->der = actual->izq;
            }
            actual->izq->padre = actual->padre;
        }
    } else {
        if (actual->izq == NULL) {
            //LA RAIZ TIENE UN HIJO A LA DERECHA
            actual->der->padre = NULL;
            _raiz = actual->der;
        } else {
            //LA RAIZ TIENE UN HIJO A LA IZQUIERDA
            actual->izq->padre = NULL;
            _raiz = actual->izq;
        }

    }
    delete actual;
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* actual = _raiz;
    while(actual != NULL && actual->valor != clave){
        if (actual->valor > clave){
            actual = actual->izq;
        } else {
            actual = actual->der;
        }
    }
    T& sig = actual->valor;
    //ASUMIMOS QUE EL ELEMENTO CLAVE SE ENCUENTRA EN EL ARBOL: actual = clave
    if(actual->der != NULL){
        //EL NODO TIENE SUBARBOL DERECHO: DEVOLVEMOS EL MINIMO DE ESTE
        actual = actual->der;
        while (actual->izq != NULL){
            actual = actual->izq;
        }
        sig = actual->valor;
    } else {
        //EL NODO NO TIENE SUBARBOL DERECHO, SEGUIMOS SUBIENDO HASTA DAR CON EL SIGUIENTE (Algoritmo de Cormen)
        Nodo* arriba = actual->padre;
        while (arriba != NULL && actual==arriba->der){
            actual = arriba;
            arriba = arriba->padre;
        }
        sig = arriba->valor;
    }
    //_setearActualEnRaiz();
    return sig;
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* r = _raiz;
    while (r->izq != NULL){
        r = r->izq;
    }
    return r->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* r = _raiz;
    while (r->der != NULL){
        r = r->der;
    }
    return r->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}
