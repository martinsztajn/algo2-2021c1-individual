
template <class T>
Conjunto<T>::Conjunto() {
    _cardinal = 0;
    _raiz = NULL;
}

template <class T>
Conjunto<T>::~Conjunto() {
    raiz();
    if(_raiz != NULL && _cardinal >0){
        _destruir(_raiz);
    }
}

template <class T>
void Conjunto<T>::_destruir(Nodo * n){
    if(n->izq != NULL){
        _destruir(n->izq);
    }
    if(n->der != NULL){
        _destruir(n->der);
    }
    delete n;
    _cardinal--;
}

template <class T>
void Conjunto<T>::raiz() {
    if (_raiz != NULL){
        while (_raiz->padre != NULL){
            _raiz = _raiz->padre;
        }
    }
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo* x = _raiz;
    while (x != NULL && x->valor != clave){
        if (clave < x->valor){
            x = x->izq;
        }
        else{
            x = x->der;
        }
    }
    return x != NULL;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    Nodo *nuevo = new Nodo(clave);
    Nodo *elemPadre = NULL;
    if(!pertenece(clave)){
        if (_cardinal == 0) {
            _raiz = nuevo;
        } else {
            while (_raiz != NULL && _raiz->valor != clave) {
                elemPadre = _raiz;
                nuevo->padre = elemPadre;
                if (_raiz->valor > clave) {
                    _raiz = _raiz->izq;
                } else {
                    _raiz = _raiz->der;
                }
            }
            if(_raiz == NULL){
                if(elemPadre->valor > clave) {
                    nuevo->padre->izq = nuevo;
                    _raiz = nuevo;
                } else {
                    nuevo->padre->der = nuevo;
                    _raiz = nuevo;
                }
            }
        }
        _cardinal++;
    }
    raiz();
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    while(_raiz != NULL && _raiz->valor != clave){
        if (_raiz->valor > clave){
            _raiz = _raiz->izq;
        } else {
            _raiz = _raiz->der;
        }
    }
    if(_raiz != NULL){
        //3 Casos
        if(_raiz->izq == NULL && _raiz->der == NULL){
            Nodo* r = _raiz;
            if(_raiz->padre != NULL){
                if (r->padre->izq == r){
                    r->padre->izq = NULL;
                } else {
                    r->padre->der = NULL;
                }
                _raiz = r->padre;
                delete r;
            } else {
                _raiz = NULL;
            }
        }
        else if (_raiz->izq == NULL || _raiz->der == NULL){
            EliminarPadreConHijo();
        } else {
            Nodo* predI = _raiz->izq;
            while(predI->der != NULL){
                predI = predI->der;
            }
            _raiz->valor = predI->valor;
            _raiz = predI;
            if(_raiz->izq == NULL && _raiz->der == NULL){
                Nodo* r = _raiz;
                if(_raiz->padre != NULL){
                    if (r->padre->izq == r){
                        r->padre->izq = NULL;
                    } else {
                        r->padre->der = NULL;
                    }
                    _raiz = r->padre;
                    delete r;
                } else {
                    _raiz = NULL;
                }            } else {
                EliminarPadreConHijo();
            }
        }
        _cardinal--;
    }
    raiz();
}


template <class T>
void Conjunto<T>::EliminarPadreConHijo() {
    Nodo *r = _raiz;
    if (_raiz->padre != NULL) {
        if (r->izq == NULL) {
            if(r->padre->izq == r){
                r->padre->izq = r->der;
            } else {
                r->padre->der = r->der;
            }
            r->der->padre = r->padre;
        } else {
            if(r->padre->izq == r){
                r->padre->izq = r->izq;
            } else {
                r->padre->der = r->izq;
            }
            r->izq->padre = r->padre;
        }
        _raiz = r->padre;
        delete r;
    } else {
        if (r->izq == NULL) {
            //LA RAIZ TIENE UN HIJO A LA DERECHA
            r->der->padre = NULL;
            r = r->der;
        } else {
            //LA RAIZ TIENE UN HIJO A LA IZQUIERDA
            r->izq->padre = NULL;
            r = r->izq;
        }
        _raiz = r;
        delete r;

    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* x = _raiz;
    while (x != NULL && x->valor != clave){
        if (clave < x->valor){
            x = x->izq;
        }
        else{
            x = x->der;
        }
    }
    if(x->der == NULL)
    {
        if (x->padre->valor > x->valor){
            return x->padre->valor;
        }
        else if(x->padre->padre->valor > x->valor) {
            return x->padre->padre->valor;
        } else{
            _raiz->valor;
        }
    }
    else{
        x = x->der;
        while (x->izq != NULL)
        {
            x = x->izq;
        }
        return x->valor;
    }
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* x = _raiz;
    while (x->izq != NULL)
    {
        x = x->izq;
    }
    return x->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* x = _raiz;
    while (x->der != NULL)
    {
        x = x->der;
    }
    return x->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}
